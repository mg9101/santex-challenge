import React, { Component } from 'react'
import Cell from '../cell'
import './_styles.scss'

class Row extends Component {
  render() {
    return (
      <div>
        {this.props.row.map((cell, i) => (
          <Cell
          key={i}
          cell={cell}
          x={this.props.x}
          y={i}
          onClick={this.props.onClick}
          onRightClick={this.props.onRightClick}
          />
        ))
        }
      </div>
    )
  }
}

export default Row;
