import React, { Component } from 'react'
import { FaBomb } from 'react-icons/fa'
import { BsFillFlagFill } from 'react-icons/bs'
import './_styles.scss'

class Cell extends Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
    this.handleRightClick = this.handleRightClick.bind(this)
  }

  handleClick(e) {
    e.preventDefault()
    this.props.onClick(this.props.x, this.props.y)
  }

  handleRightClick(e) {
    e.preventDefault()
    this.props.onRightClick(this.props.x, this.props.y)
  }

  render() {
    let content = this.props.cell.flagged ? <BsFillFlagFill /> : ''

    if (this.props.cell.open) {
      if (this.props.cell.hasBomb) {
        content = (
          <div className="cell--bomb">
            <FaBomb/>
          </div>
        );
      } else {
        content = (this.props.cell.adjacentBombs > 0) ? this.props.cell.adjacentBombs : '';
      }
    }

    return (
      <div
        className={this.props.cell.open ? 'cell cell--opened' : 'cell'}
        onClick={this.handleClick}
        onDoubleClick={this._handleDoubleClick}
        onContextMenu={this.handleRightClick}
      >
        {content}
      </div>
    )
  }
}

export default Cell;
