import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import CustomLayout from './components/custom-layout';

ReactDOM.render(
  <React.StrictMode>
    <CustomLayout />
  </React.StrictMode>,
  document.getElementById('root')
);
