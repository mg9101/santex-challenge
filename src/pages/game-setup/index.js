import React, { useState, useEffect } from 'react';
import config from '../../config'
import './_styles.scss';

function GameSetup() {
  const [value, setValue] = useState(localStorage.getItem('difficulty') || 'easy');
  const [customConfig, setCustomConfig] = useState(config);

  useEffect(() => {
    console.log('updating custom config', customConfig);
    localStorage.setItem('customConfig', JSON.stringify(customConfig));
  }, [customConfig]);

  const changeDifficulty = value => setValue(value);

  const changeCustomConfig = e => {
    customConfig[value][e.target.name] = e.target.value;
    console.log(customConfig);
    setCustomConfig(customConfig);
  };

  return (
    <div className="game-setup">
      <h2>Settings</h2>
      <div className="game-setup__item">
        <div className="option-button">
          <div onClick={() => changeDifficulty('easy')} className={`option-button__item ${(value === 'easy') ? 'option-button__item--selected' : ''}`}>
            Easy
          </div>
          <div onClick={() => changeDifficulty('medium')} className={`option-button__item ${(value === 'medium') ? 'option-button__item--selected' : ''}`}>
            Medium
          </div>
          <div onClick={() => changeDifficulty('hard')} className={`option-button__item ${(value === 'hard') ? 'option-button__item--selected' : ''}`}>
            Hard
          </div>
        </div>
      </div>
      <div className="game-setup__item">
        <label className="item__title">Grid width (cell count on the x ascis)</label>
        <input className="item__input" name="gridWidth" value={customConfig[value].gridWidth} onChange={changeCustomConfig}/>
      </div>
      <div className="game-setup__item">
        <label className="item__title">Grid heigth (cell count on the y ascis)</label>
        <input className="item__input" name="gridHeight" value={customConfig[value].gridHeight} onChange={changeCustomConfig}/>
      </div>
      <div className="game-setup__item">
        <label className="item__title">Bomb quantity</label>
        <input className="item__input" name="bombNum" value={customConfig[value].bombNum} onChange={changeCustomConfig}/>
      </div>
    </div>
  );
}

export default GameSetup;
