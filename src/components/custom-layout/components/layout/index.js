import React from 'react';
import Header from '../header';
import {
  HashRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Minesweeper from '../../../../pages/minesweeper';
import GameSetup from '../../../../pages/game-setup';

const Layout = props => (
  <>
    <div className="layout">
      <div className="layout__sidebar">
        <div className="sidebar">
          <div className="sidebar__logo">
            {/* <img src={logo} className="sidebar__logo--belt" alt="belt admin"/> */}
          </div>
          <Router>
            <div>
              <nav>
                <ul>
                  <li className="sidebar__item">
                    <Link to="/" className="sidebar__item--link" >Play</Link>
                  </li>
                  <li className="sidebar__item">
                    <Link to="/game-setup" className="sidebar__item--link" >Setup</Link>
                  </li>
                </ul>
              </nav>
            </div>
          </Router>
        </div>
      </div>
      <div className="layout__content">
        <Header
        />
        <div className="layout__container">
          <Router>
              <Switch>
                <Route path="/game-setup">
                  <GameSetup />
                </Route>
                <Route path="/">
                  <Minesweeper />
                </Route>
              </Switch>
          </Router>
        </div>
      </div>
    </div>
  </>
);

export default Layout;
