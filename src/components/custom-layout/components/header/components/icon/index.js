/* eslint-disable max-len */
const React = require('react');
const PropTypes = require('prop-types');

const Chevron = props => (
  <svg className={`icon ${props.className}`} width="16" height="16" viewBox="0 0 16 16">
    <defs>
      <path id="a" d="M5.25 9.796L8.847 6.2l-.849-.849-4.445 4.445 4.449 4.449.848-.849z" />
    </defs>
    <use fill="#000" fillOpacity=".45" fillRule="nonzero" xlinkHref="#a" />
  </svg>
);

Chevron.propTypes = {
  className: PropTypes.string,
};

Chevron.defaultProps = {
  className: '',
};

const Search = props => (
  <svg className={`icon ${props.className}`} width="16" height="16" viewBox="0 0 16 16">
    <path fill="#999" fillOpacity=".8" fillRule="nonzero" d="M6.2.2c3.314 0 6 2.686 6 6 0 1.44-.507 2.763-1.354 3.797l4.002 4.002-.849.848-4.001-4.002C8.964 11.692 7.64 12.2 6.2 12.2c-3.314 0-6-2.687-6-6 0-3.314 2.686-6 6-6zm0 1.2c-2.65 0-4.799 2.149-4.799 4.8 0 2.65 2.149 4.8 4.8 4.8C8.85 11 11 8.85 11 6.2c0-2.651-2.15-4.8-4.8-4.8z" />
  </svg>
);

Search.propTypes = {
  className: PropTypes.string,
};

Search.defaultProps = {
  className: '',
};

const Account = props => (
  <svg className={`icon ${props.className}`} width="20" height="20" viewBox="0 0 20 20">
    <path fill="#000" fillOpacity=".8" fillRule="nonzero" d="M10 1.003c4.97 0 9 4.03 9 9s-4.03 9-9 9-9-4.03-9-9 4.03-9 9-9zM13.602 14.2H6.383c-.768 0-1.39.623-1.39 1.391v.392c1.354 1.136 3.1 1.82 5.007 1.82 1.906 0 3.652-.684 5.007-1.819v-.38c0-.775-.63-1.404-1.405-1.404zM10 2.203c-4.308 0-7.8 3.492-7.8 7.8 0 1.837.635 3.525 1.697 4.858C4.212 13.785 5.207 13 6.383 13h7.22c1.18 0 2.178.787 2.497 1.865 1.064-1.333 1.7-3.023 1.7-4.862 0-4.308-3.492-7.8-7.8-7.8zm.006 1.192c2.32 0 4.202 1.882 4.202 4.203 0 2.32-1.881 4.202-4.202 4.202-2.321 0-4.203-1.882-4.203-4.203 0-2.32 1.882-4.202 4.203-4.202zm0 1.2c-1.658 0-3.003 1.344-3.003 3.003 0 1.658 1.345 3.002 3.003 3.002 1.658 0 3.002-1.344 3.002-3.002 0-1.659-1.344-3.003-3.002-3.003z" />
  </svg>
);

Account.propTypes = {
  className: PropTypes.string,
};

Account.defaultProps = {
  className: '',
};

const Help = props => (
  <svg className={`icon ${props.className}`} width="20" height="20" viewBox="0 0 20 20">
    <path fill="#000" fillOpacity=".8" fillRule="nonzero" d="M10 1c4.97 0 9 4.03 9 9s-4.03 9-9 9-9-4.03-9-9 4.03-9 9-9zm0 1.2c-4.308 0-7.8 3.492-7.8 7.8s3.492 7.8 7.8 7.8 7.8-3.492 7.8-7.8-3.492-7.8-7.8-7.8zm.023 10.86c.57 0 1.05.48 1.05 1.05 0 .57-.48 1.05-1.05 1.05-.57 0-1.05-.48-1.05-1.05 0-.57.48-1.05 1.05-1.05zm.09-8.22c1.89 0 3.045 1.005 3.045 2.355 0 1.245-.84 1.845-1.59 2.37-.57.42-1.095.78-1.095 1.38 0 .24.134.51.33.675l-1.32.435c-.36-.345-.54-.78-.54-1.275 0-1.005.72-1.515 1.364-1.98.556-.39 1.05-.735 1.05-1.32 0-.585-.45-1.08-1.425-1.08-.915 0-1.575.42-2.085 1.05L6.843 6.325c.766-.93 1.92-1.485 3.27-1.485z" />
  </svg>
);

Help.propTypes = {
  className: PropTypes.string,
};

Help.defaultProps = {
  className: '',
};

const Laptop = props => (
  <svg className={`icon ${props.className}`} width="35" height="35" viewBox="0 0 512 512">
    <path fill="#1E2477" d="M512 416v-.2l-.2-.8v-.1l-.2-.8v-.2l-.4-.6v-.2l-40.1-79V73.6c0-8-6.5-14.5-14.5-14.5H55.4c-8 0-14.5 6.5-14.5 14.5V334L1 413.1l-.1.2-.3.6-.1.2-.2.8-.2 1v19.6c0 9.6 7.7 17.3 17.2 17.3h477.6c9.5 0 17.2-7.7 17.2-17.2v-19-.5zM56 74.3h400v254.1H56V74.2zm-3 269.1h406l33.3 65.7H327.5l-8.3-30a14.2 14.2 0 00-13.6-10.3h-99.2c-6.4 0-12 4.2-13.6 10.3l-8.3 30H19.7L53 343.3zM312 409H200l7-25.3h98l7 25.3zm185 26.6c0 1.2-1 2.2-2.2 2.2H17.2c-1.2 0-2.2-1-2.2-2.2V424h482v11.6z" />
    <path fill="#1E2477" d="M432.6 213.8a7.5 7.5 0 00-7.5 7.5v76H86.9v-76a7.5 7.5 0 10-15 0v79.3c0 6.4 5.3 11.7 11.8 11.7h344.6c6.5 0 11.8-5.3 11.8-11.7v-79.3c0-4.2-3.4-7.5-7.5-7.5zM428.3 90.2H83.7c-6.5 0-11.8 5.3-11.8 11.8v79.3a7.5 7.5 0 1015 0v-76.1h338.2v76a7.5 7.5 0 1015 0V102c0-6.5-5.3-11.8-11.8-11.8z" />
  </svg>
);

Laptop.propTypes = {
  className: PropTypes.string,
};

Laptop.defaultProps = {
  className: '',
};


module.exports = {
  Chevron,
  Search,
  Account,
  Help,
  Laptop,
};
