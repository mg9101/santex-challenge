import React, { Component } from 'react'
import Row from '../row'
import './_styles.scss'

class Grid extends Component {
  render() {
    return (
      <div className="grid">
        {
          this.props.grid.map((row, i) => (
            <Row
            key={i}
            row={row}
            x={i}
            onClick={this.props.onClick}
            onRightClick={this.props.onRightClick}
          />
          ))
        }
      </div>
    )
  }
}

export default Grid;
