import React from 'react';
import Layout from './components/layout';
import './styles.scss';

const CustomLayout = props => (
  <main className="ui-content" role="main" id="root-app">
    <Layout>
      {props.children}
    </Layout>
  </main>
);

export default CustomLayout;
