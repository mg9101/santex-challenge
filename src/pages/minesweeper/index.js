import React, { Component } from 'react'
import Grid from './components/grid'
import config from '../../config'
import './styles.scss'

const customConfig = JSON.parse(localStorage.getItem('customConfig')) || config;
const savedGame = JSON.parse(localStorage.getItem('savedGame')) || false;

class Minesweeper extends Component {
  constructor(props) {
    super(props)
    const difficulty = localStorage.getItem('difficulty') || 'easy';
    const { bombNum } = customConfig[difficulty]
    this.state = {
      difficulty,
      customConfig,
      gameOver: false,
      win: false,
      flagCount: bombNum,
      grid: savedGame || this.initializeGrid(difficulty)
    }
    this.handleClick = this.handleClick.bind(this)
    this.handleClickCell = this.handleClickCell.bind(this)
    this.handleRightClickCell = this.handleRightClickCell.bind(this)
    this.saveGame = this.saveGame.bind(this)
  }

  handleClick(e) {
    e.preventDefault()
    const { bombNum } = config[this.state.difficulty]
    this.setState((prevState) => ({
      gameOver: false,
      flagCount: bombNum,
      grid: this.initializeGrid(prevState.difficulty),
    }))
  }

  handleClickCell(x, y) {
    const { gameOver, win } = this.state
    if (!gameOver && !win) {
      this.openTile(x, y)
    }
  }

  handleRightClickCell(x, y) {
    const { gameOver, win } = this.state;
    if (gameOver || win) {
      return;
    }

    this.toggleFlag(x, y);
  }

  initializeGrid(difficulty) {
    const bombPlaces = this.initializeBombs(difficulty)
    const { gridWidth, gridHeight } = config[difficulty]
    const grid = [];
    for (let i = 0; i < gridWidth; i++) {
      grid[i] = [];
      for (let j = 0; j < gridHeight; j++) {
        grid[i][j] = { open: false, flagged: false, hasBomb: false, adjacentBombs: 0 }
      }
    }

    for (let place of bombPlaces) {
      grid[place.x][place.y].hasBomb = true;
    }
    return grid
  }

  initializeBombs(difficulty) {
    const bombPlaces = []
    const { gridWidth, gridHeight, bombNum } = config[difficulty]

    while (bombPlaces.length < bombNum) {
      const x = Math.floor(Math.random() * gridWidth)
      const y = Math.floor(Math.random() * gridHeight)
      if (bombPlaces.length === 0) {
        bombPlaces.push({ x: x, y: y })
      } else {
        const duplicated = bombPlaces.filter((place) => {
          return place.x === x && place.y === y
        }).length > 0
        if (!duplicated) {
          bombPlaces.push({ x: x, y: y })
        }
      }
    }
    return bombPlaces
  }

  changeDifficulty(e) {
    const difficulty = e.target.value
    const { bombNum } = config[difficulty]

    this.setState({
      difficulty,
      flagCount: bombNum,
      grid: this.initializeGrid(difficulty)
    })
  }

  openTile(x, y) {
    const grid = this.state.grid;
    const { gridWidth, gridHeight } = config[this.state.difficulty]
    if (!grid[x][y].open && !grid[x][y].flagged) {
      let adjacentBombs = 0
      for (let i = x - 1; i <= x + 1; i++) {
        for (let j = y - 1; j <= y + 1; j++) {
          if ((i < 0 || i >= gridWidth) ||
              (j < 0 || j >= gridHeight) ||
              (i === x && j === y)) {
            continue
          }
          if (grid[i][j].hasBomb) {
            adjacentBombs++
          }
        }
      }

      grid[x][y].open= true;
      grid[x][y].adjacentBombs= adjacentBombs;
      this.setState({ grid })

      if (grid[x][y].hasBomb) {
        this.gameover();
      }

      if (this.hasWon(grid)) {
        this.win();
      }

      if (adjacentBombs === 0 && !grid[x][y].hasBomb) {
        this.openEmptyTiles(x, y);
      }
    }
  }

  hasWon(grid) {
    let openCount = 0
    const { gridWidth, gridHeight, bombNum } = config[this.state.difficulty]
    grid.forEach((row, i) => {
      row.forEach((cell, i) => {
        if (cell.open) {
          openCount++
        }
      })
    })
    return openCount === (gridWidth * gridHeight - bombNum)
  }

  toggleFlag(x, y) {
    const grid = this.state.grid;
    const { flagged } = grid[x][y];
    grid[x][y].flagged = !flagged;
    this.setState((prevState) => ({
      grid,
      flagCount: (flagged) ? prevState.flagCount + 1 : prevState.flagCount - 1,
    }))
  }

  gameover(){
    this.setState({
      gameOver: true,
    })
  }

  win(){
    this.setState({
      win: true,
    })
  }

  saveGame(){
    localStorage.setItem('savedGame', JSON.stringify(this.state.grid));
  }

  loadGame(){
    localStorage.setItem('savedGame', JSON.stringify(this.state.grid));
  }

  openEmptyTiles(x, y) {
    const grid = this.state.grid;
    const { gridWidth, gridHeight } = config[this.state.difficulty]

    for (let i = x - 1; i <= x + 1; i++) {
      for (let j = y - 1; j <= y + 1; j++) {
        if ((i < 0 || i >= gridWidth) ||
            (j < 0 || j >= gridHeight) ||
            (i === x && j === y) ||
            (grid[i][j].flagged)) {
          continue
        }
        this.openTile(i, j)
      }
    }
  }

  render() {
    return (
      <div className="minesweeper">
        <h1>Minesweeper</h1>
        <div className="minesweeper__controls">
          <button className="controls__item controls__btn" onClick={this.handleClick}>New Game</button>
          <button className="controls__item controls__btn" onClick={this.saveGame}>Save Game</button>
          <select className="controls__item" value={this.state.difficulty} onChange={(e) => this.changeDifficulty(e)}>
              <option value={'easy'} key={'easy'}>Easy</option>
              <option value={'medium'} key={'medium'}>Medium</option>
              <option value={'hard'} key={'hard'}>Hard</option>
            </select>
          <div className="controls__item controls__bombsLeft">Bombs left <span>{this.state.flagCount}</span></div>
        </div>
        <Grid
          grid={this.state.grid}
          onClick={this.handleClickCell}
          onRightClick={this.handleRightClickCell}
        />
      </div>
    )
  }
}

export default Minesweeper
