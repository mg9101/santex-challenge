const config = {
  easy: {
    gridWidth: 8,
    gridHeight: 8,
    bombNum: 10,
  },
  medium: {
    gridWidth: 16,
    gridHeight: 16,
    bombNum: 40,
  },
  hard: {
    gridWidth: 30,
    gridHeight: 16,
    bombNum: 99,
  },
}

export default config
