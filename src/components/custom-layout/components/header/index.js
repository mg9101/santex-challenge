/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/click-events-have-key-events */
const React = require('react');

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isSpinnerVisible: false,
    };

    this.searchInput = React.createRef();

  }

  render() {
    return (
      <header role="banner">
        <div className="header">
          <div className="header__content">
            <div className="header__container">
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
